package co.bytemark.android.opentools.autoresizer;

import android.app.Activity;
import android.view.Display;


/**
 * <b>Location:</b>
 * <br>AutoResizer.java must be located within co.bytemark.android.opentools.autoresizer to be compatible with other Bytemark inc. opentools. 
 * <br>
 * <br><b>Description</b>
 * <br>AutoResizer is a Bytemark inc. opentool that will assist in creating uniform layouts across all screen sizes for android devices.
 * <br>
 * <br>Before use, AutoResizer must receive a Display object (via <code>setDisplaySize</code>) from the current activity at the beginning of it's onCreate(). 
 * <br>
 * <br>The AutoResizer functions by returning a pixel size adjusted against the ratio of the current screen size to that of an iPhone 3Gs. The AutoResizer can adjust both the size in pixels of a standardized screen size and a percentage of the screen. 
 */

public class AutoResizer {
	static String TAG = "AutoResizer";
	public static int m_nCurrScreenWidth;
	public static int m_nCurrScreenHeight;
	
	/*Original designs were based on a 320 x 480 (iPhone 3G) screen */
	public static float m_fOriginalHeight = 480.0f;
	public static float m_fOriginalWidth = 320.0f;
	
	public static final float m_roundUp = 0.5f; //casting to an int, this ensures that the value rounds up when it has to


	/**
	 * Sets the current phone's pixel height and width to autoresizer, so we can calculate the new size of views accordingly.
	 * 
	 * @param display
	 */
	@SuppressWarnings("deprecation")
	public static void setDisplaySize (Display display){
		m_nCurrScreenWidth = display.getWidth();
		m_nCurrScreenHeight = display.getHeight();
	}
	
	public static void setDisplayFromActivity (Activity activity){
		AutoResizer.setDisplaySize(activity.getWindowManager().getDefaultDisplay());
	}
	
	public static String printString(){
		return "m_nScreenWidth: " + m_nCurrScreenWidth + " m_nScreenHeight: " + m_nCurrScreenHeight;
	}
	
	public static int resizeRelativeToOriginalWidth(float originalsize){
		return AutoResizer.ratioBySize(originalsize, m_fOriginalWidth, m_nCurrScreenWidth);
		
	}
	
	public static int resizeRelativeToOriginalHeight(float originalsize){
		return AutoResizer.ratioBySize(originalsize, m_fOriginalHeight, m_nCurrScreenHeight); 

	}

	
	/**
	 * We calculate the original proportion of the original view relative to the original screen 
	 * <br> (ie: 30/320 is 30px on a 320px width screen)
	 * <br>
	 * <br>Then, we multiply that proportion by the current screen size, to get the new view's size . 
	 * <br> (ie. 480 * (30/320) = 45) or (30/320 = 45/480)
	 * <br>
	 * <br>We add <code>.5f</code> to the total in order to insure that it rounds up if it has to, if we're casting to an int. 
	 * 
	 * @param originalViewSize
	 * @param originalScreenPxSize
	 * @return
	 */
	public static int ratioBySize (float originalViewSize, float originalScreenPxSize, int currScreenPxSize){
		AutoResizer.warnIfDisplayNotSet();
		return (int) ((currScreenPxSize * (originalViewSize/originalScreenPxSize)) + m_roundUp);  
	}

	
	/**
	 * @Category Scale by percent.
	 */

	
	
	
	public static int resizeToPercentOfScreenWidth (float _fPercentWidth){
		return scaleByPercent(m_nCurrScreenWidth, _fPercentWidth);
	}
	
	public static int resizeToPercentOfScreenHeight (float _fPercentHeight){
		return scaleByPercent(m_nCurrScreenHeight, _fPercentHeight);
	}
	
	
	public static int scaleByPercent (int currentScreenSize, float percent){		
		AutoResizer.warnIfDisplayNotSet();
		return (int) (currentScreenSize*percent + m_roundUp);
	}
	
	public static void warnIfDisplayNotSet (){
		if ((m_nCurrScreenWidth==0) && (m_nCurrScreenHeight == 0)){
			System.err.println("WARNING: The AutoResizer Display has not been set.");
		}
	}

}
